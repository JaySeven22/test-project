import {reactive, watch} from 'vue';


export const initItems = [
    {
        title: '«Рождение Венеры» Сандро Боттичелли',
        image: require('@/assets/images/painting-63186_1280 1.jpg'),
        cost: '1 000 000 $',
        discount: "2 000 000 $",
        isInCart: true
    },
    {
        title: '«Тайная вечеря» Леонардо да Винчи',
        image: require('@/assets/images/ae973f6678e037cd297053384aa7dca0 1.jpg'),
        cost: '3 000 000 $',
        isInCart: true 
    },
    {
        title: '«Сотворение Адама» Микеланджело',
        image: require('@/assets/images/image-19 1.jpg'),
        cost: '5 000 000 $',
        discount: '6 000 000 $',
        isInCart: true
    },
    {
        title: '«Урок анатомии» Рембрандт',
        image: require('@/assets/images/20152310142330 1.jpg'),
        cost: 'Продана на аукционе',
        isInCart: true
    },
 ]

const initState = JSON.parse(localStorage.getItem('state'));

export const store = reactive(initState ||  {
    items : initItems ,
    selective: null 
});

watch(() => store, store => {
    localStorage.setItem('state', JSON.stringify(store))
},{deep: true})